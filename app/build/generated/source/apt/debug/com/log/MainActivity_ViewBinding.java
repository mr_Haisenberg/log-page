// Generated code from Butter Knife. Do not modify!
package com.log;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131230765;

  private View view2131230758;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.imageView = Utils.findRequiredViewAsType(source, R.id.imageView, "field 'imageView'", ImageView.class);
    target.txtInputLogin = Utils.findRequiredViewAsType(source, R.id.login, "field 'txtInputLogin'", TextInputEditText.class);
    target.txtInputPass = Utils.findRequiredViewAsType(source, R.id.pass, "field 'txtInputPass'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.chBoxRem, "field 'chBoxRem' and method 'saveData'");
    target.chBoxRem = Utils.castView(view, R.id.chBoxRem, "field 'chBoxRem'", CheckBox.class);
    view2131230765 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.saveData();
      }
    });
    view = Utils.findRequiredView(source, R.id.btnLogIn, "field 'btnLogIn' and method 'submit'");
    target.btnLogIn = Utils.castView(view, R.id.btnLogIn, "field 'btnLogIn'", Button.class);
    view2131230758 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    target.txtGoToReg = Utils.findRequiredViewAsType(source, R.id.txtGoToReg, "field 'txtGoToReg'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imageView = null;
    target.txtInputLogin = null;
    target.txtInputPass = null;
    target.chBoxRem = null;
    target.btnLogIn = null;
    target.txtGoToReg = null;

    view2131230765.setOnClickListener(null);
    view2131230765 = null;
    view2131230758.setOnClickListener(null);
    view2131230758 = null;
  }
}
