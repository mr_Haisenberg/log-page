package com.log;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity  {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.login)
    TextInputEditText txtInputLogin;
    @BindView(R.id.pass)
    TextInputEditText txtInputPass;
    @BindView(R.id.chBoxRem)
    CheckBox chBoxRem;
    @BindView(R.id.btnLogIn)
    Button btnLogIn;
    @BindView(R.id.txtGoToReg)
    TextView txtGoToReg;

    String login,pass;
    Boolean saveLogin;
    SharedPreferences sPref;
    SharedPreferences.Editor ed;

    public static final String DATA_LOG = "login";
    public static final String DATA_PASS = "pass";
    public static final String NAME_PREFERENCES = "saveLogin";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setDrawable();
        setSaveLogin();
        setClickSignUp();



    }
    private void setDrawable() {
        imageView.setImageResource(R.drawable.shape);
        imageView.setBackgroundResource(R.color.trans);
    }

    private void setSaveLogin() {
        sPref = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        ed = sPref.edit();
        saveLogin = sPref.getBoolean(NAME_PREFERENCES, false);
        if (saveLogin == true) {
            txtInputLogin.setText(sPref.getString(DATA_LOG, ""));
            txtInputPass.setText(sPref.getString(DATA_PASS, ""));
            chBoxRem.setChecked(true);
        }
    }
    public void setClickSignUp(){
        String signUp = "Sign up";
        txtGoToReg.setText(String.format("Need an account? " + "Sign up", signUp));
        txtGoToReg.setMovementMethod(LinkMovementMethod.getInstance());
        txtGoToReg.setTextColor(Color.YELLOW);
        Pattern signUpMatcher = Pattern.compile(signUp);
        Linkify.addLinks(txtGoToReg,signUpMatcher,"Sign :");

    }
    @Override
    public void startActivity(Intent intent){
        if(TextUtils.equals(intent.getAction(),Intent.ACTION_VIEW)){
            startActivity(new Intent(getApplicationContext(),RegistrationActivity.class));
        }
        else {
            super.startActivity(intent);
        }
    }


    @OnClick(R.id.btnLogIn)
    public void submit(View view) {
        if(txtInputPass.getText().toString().equals("admin") && txtInputLogin.getText().toString().equals("admin")){
            Toast.makeText(getApplicationContext(),"Next Activity",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,FirstPageActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.chBoxRem)
    public void saveData(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtInputLogin.getWindowToken(), 0);
        login = txtInputLogin.getText().toString();
        pass = txtInputPass.getText().toString();
        if (chBoxRem.isChecked()) {
            Toast.makeText(getApplicationContext(),"Save data",Toast.LENGTH_LONG).show();
            ed.putBoolean(NAME_PREFERENCES, true);
            ed.putString(DATA_LOG, login);
            ed.putString(DATA_PASS, pass);
            ed.commit();
        }
        else {
            Toast.makeText(getApplicationContext(),"Nothing save",Toast.LENGTH_LONG).show();
            ed.clear();
            ed.commit();
        }
    }

}
