package com.log;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegistrationActivity extends AppCompatActivity {
    @BindView(R.id.imageViewReg)
    ImageView imageViewReg;
    @BindView(R.id.txtInputNewLogin)
    TextInputEditText txtInputNewLogin;
    @BindView(R.id.txtInputNewPass)
    TextInputEditText txtInputNewPass;
    @BindView(R.id.txtInputConfirmNewPass)
    TextInputEditText txtInputConfirmNewPass;
    @BindView(R.id.txtGoToLog)
    TextView txtGoToLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        setDrawable();
        setClickSignIn();
    }

    private void setDrawable() {
        imageViewReg.setImageResource(R.drawable.shape);
        imageViewReg.setBackgroundResource(R.color.trans);
    }
    public void setClickSignIn(){
        String signIn = "Sign in";
        txtGoToLog.setText(String.format("Have an account? " + "Sign in", signIn));
        txtGoToLog.setMovementMethod(LinkMovementMethod.getInstance());
        txtGoToLog.setTextColor(Color.YELLOW);
        Pattern signUpMatcher = Pattern.compile(signIn);
        Linkify.addLinks(txtGoToLog,signUpMatcher,"Sign :");

    }
    @Override
    public void startActivity(Intent intent){
        if(TextUtils.equals(intent.getAction(),Intent.ACTION_VIEW)){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }
        else {
            super.startActivity(intent);
        }
    }


}
