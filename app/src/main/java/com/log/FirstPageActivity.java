package com.log;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class FirstPageActivity extends AppCompatActivity {
    @BindView(R.id.btnDepositCalc)
    Button btnDepositCalc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);
        ButterKnife.bind(this);



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main,menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();
        if (id==R.id.logOut){
            Toast.makeText(getApplicationContext(),"Log out",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);}

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnDepositCalc)
    public void setDepositCalc(View view) {
        Intent intent = new Intent(this,DepositCalculator.class);
        startActivity(intent);
    }






}
