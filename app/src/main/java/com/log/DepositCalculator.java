package com.log;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DepositCalculator extends AppCompatActivity {
    @BindView(R.id.btnDepositCalc)
    Button btnDepositCalculate;
    @BindView(R.id.txtInputSum)
    TextInputEditText txtInputSum;
    @BindView(R.id.txtInputProc)
    TextInputEditText txtInputProc;
    @BindView(R.id.txtInputStrok)
    TextInputEditText txtInputStrok;
    @BindView(R.id.tvText)
    TextView tvText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit_calculator);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.btnDepositCalc)
    public void depositCalc(View view){

        float res = 0;
        float sum = 0;
        float proc = 0;
        sum = Float.parseFloat(txtInputSum.getText().toString());
        proc = Float.parseFloat(txtInputProc.getText().toString());
        // days = Integer.getInteger(txtInputStrok.getText().toString());
        res = sum+proc;
        tvText.setText(sum+"");

    }
}
